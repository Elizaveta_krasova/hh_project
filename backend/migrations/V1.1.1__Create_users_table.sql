CREATE TABLE IF NOT EXISTS users
(
    id       integer PRIMARY KEY
        UNIQUE
                  NOT NULL GENERATED ALWAYS AS IDENTITY,
    name            text NOT NULL,
    lastname        text NOT NULL,
    login           text NOT NULL,
    password        text NOT NULL,
    phone_number    text NOT NULL,
    birthday        date NOT NULL,
    joined          date NOT NULL,
    address         text NOT NULL,
    points          int NOT NULL
);
