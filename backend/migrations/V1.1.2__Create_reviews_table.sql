CREATE TABLE IF NOT EXISTS reviews
(
    id          integer PRIMARY KEY
        UNIQUE
                    NOT NULL GENERATED ALWAYS AS IDENTITY,
    topic           TEXT NOT NULL,
    description     TEXT NOT NULL,
    author_id       INT REFERENCES users NOT NULL,
    addressee_user_id         INT REFERENCES users NOT NULL
);