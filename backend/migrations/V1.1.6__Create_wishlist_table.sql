CREATE TABLE IF NOT EXISTS wishlist
(
    user_id     INT REFERENCES users NOT NULL,
    post_id     INT REFERENCES posts NOT NULL
);