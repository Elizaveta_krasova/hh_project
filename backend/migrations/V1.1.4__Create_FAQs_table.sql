CREATE TABLE IF NOT EXISTS FAQs
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    question        TEXT NOT NULL,
    answer          TEXT NOT NULL
);