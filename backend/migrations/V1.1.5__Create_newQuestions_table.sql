CREATE TABLE IF NOT EXISTS newQuestions
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    name             TEXT NOT NULL,
    email            TEXT NOT NULL,
    message          TEXT NOT NULL,
    is_answered      BOOLEAN
);
