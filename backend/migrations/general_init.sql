DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS reviews CASCADE;
DROP TABLE IF EXISTS posts CASCADE;
DROP TABLE IF EXISTS wishlist CASCADE;
DROP TABLE IF EXISTS orders CASCADE;

CREATE TABLE IF NOT EXISTS users
(
    id       integer PRIMARY KEY
        UNIQUE
                  NOT NULL GENERATED ALWAYS AS IDENTITY,
    name            text NOT NULL,
    login           text NOT NULL,
    password        text NOT NULL,
    phone           text,
    date_creation   timestamp
);

CREATE TABLE IF NOT EXISTS reviews
(
    id          integer PRIMARY KEY
        UNIQUE
                    NOT NULL GENERATED ALWAYS AS IDENTITY,
    description     TEXT NOT NULL,
    author_id       INT REFERENCES users NOT NULL,
    addressee_user_id         INT REFERENCES users NOT NULL,
    date_creation   timestamp
);

CREATE TABLE IF NOT EXISTS posts
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    type        TEXT,
    name        TEXT NOT NULL,
    description TEXT NOT NULL,
    schedule    timestamp,
    author_id     INT REFERENCES users NOT NULL,
    date_creation   timestamp
--    check (type in ('employee', 'employer'))
);

CREATE TABLE IF NOT EXISTS wishlist
(
    user_id     INT REFERENCES users NOT NULL,
    post_id     INT REFERENCES posts NOT NULL
);


-- author_response_type: "Отклонили", "Приняли", "На рассмотрении"
CREATE TABLE IF NOT EXISTS orders
(
    user_id     INT REFERENCES users NOT NULL,
    post_id     INT REFERENCES posts NOT NULL,
    motivation_letter TEXT,
    author_response_type TEXT
);


