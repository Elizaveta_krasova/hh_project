from flask import Flask, render_template, url_for, request, redirect, session, jsonify
from flask_cors import CORS, cross_origin
import os
import psycopg
from dotenv import load_dotenv
import i18n
import logging
import json

load_dotenv()

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


conn = psycopg.connect(dbname=os.getenv('DB_NAME'),
                       user=os.getenv('DB_USER'),
                       password=os.getenv('DB_PASSWORD'),
                       host=os.getenv('DB_HOST'),
                       port=os.getenv('DB_PORT'))
conn.autocommit = True

cur = conn.cursor()


# Создание пользователя
# Возвращает ID созданного пользователя
# BODY
# {
#   "name": text
#   "login": text
#   "password": text
#   "phone": text
#   "date_creation": timestamp ('2004-10-19 10:23:54')
# }
@app.route("/api/create_user", methods=["POST"])
def create_user():
    try:
        body = request.json
        name = body['name']
        login = body['login']
        password = body['password']
        phone = body['phone']
        date_creation = body['date_creation']
        query = """
                    INSERT INTO users (name, login, password, phone, date_creation) 
                    VALUES ( %s, %s, %s, %s, %s)
                    RETURNING id;
                    """
        cur.execute(query, (name, login, password, phone, date_creation))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Вход в аккаунт. Выдаст 400, если неправильно что-то
# Возвращает ID пользователя
# BODY
# {
#   "login": text
#   "password": text
# }
@app.route("/api/login", methods=["POST"])
def login():
    try:
        body = request.json
        login = body['login']
        password = body['password']
        query = """
                    SELECT id FROM users 
                    WHERE login = %s and password = %s;
                """
        cur.execute(query, (login, password))
        user = cur.fetchall()
        if user:
            return user, 200, {'content-type': 'text/json'}
        else:
            return {'message': 'Bad Request'}, 400
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Выдаёт информацию о пользователе по его ID
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_by_id", methods=["POST"])
def get_user_by_id():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT name, date_creation, phone FROM users WHERE id = %s;"""
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отдаёт все все все посты
@app.route("/api/get_all_posts", methods=["GET"])
def get_all_posts():
    try:
        query = """SELECT * FROM posts;"""
        cur.execute(query)
        array_search = cur.fetchall()
        return array_search, 200
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отдаёт посты с опеределенным типом. Либо "Соискателям" (employee), либо "Работодателям" (employeer)
# BODY
# {
#   "type": "employee or employeer"
# }
@app.route("/api/get_posts", methods=["POST"])
def get_posts():
    try:
        body = request.json
        type = body['type']
        query = f"""SELECT * FROM posts WHERE type = %s;"""
        cur.execute(query, (type, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отдаёт посты пользователя
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_posts", methods=["POST"])
def get_user_posts():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT posts.* FROM posts 
                    left join users on users.id = author_id 
                    WHERE author_id = %s;
                """
        with conn.cursor() as cur:
            cur.execute(query, (id, ))
            array_search = cur.fetchall()
            return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод показывает людей, которые откликнулись на пост (Если автор поставил пользователю "Отклонить", то его заявка больше не будет показываться)
# "author_response_type": "Отклонили", "Приняли", "На рассмотрении". При отклике автоматически ставится "На рассмотрении"
# BODY
# {
#   "id": int
# }
@app.route("/api/get_response_posts", methods=["POST"])
def get_response_posts():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT users.name, orders.motivation_letter, orders.author_response_type FROM posts 
                    left join orders on posts.id = post_id 
                    left join users on users.id = user_id 
                    WHERE author_response_type = 'Приняли' OR author_response_type = 'На рассмотрении';
                """
        cur.execute(query)
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400



# Метод отдаёт пост по его ID
# BODY
# {
#   "id": int
# }
@app.route("/api/get_post_by_id", methods=["POST"])
def get_post_by_id():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT posts.id, posts.type, posts.name, description, schedule, users.name 
                    FROM posts 
                    left join users on users.id = author_id 
                    WHERE posts.id = %s;
                """
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод создаёт пост
# Возвращает ID созданного поста
# BODY
# {
#   "type": "employee or employeer"
#   "name": text
#   "description": text
#   "schedule": timestamp ('2004-10-19 10:23:54')
#   "author_id": int
#   "date_creation": timestamp ('2004-10-19 10:23:54')
# }
@app.route("/api/create_post", methods=["POST"])
def create_post():
    try:
        body = request.json
        type = body['type']
        name = body['name']
        description = body['description']
        schedule = body['schedule']
        author_id = body['author_id']
        date_creation = body['date_creation']
        query = """
                    INSERT INTO posts (type, name, description, schedule, author_id, date_creation) 
                    VALUES ( %s, %s, %s, %s, %s, %s)
                    RETURNING id;
                """
        cur.execute(query, (type, name, description, schedule, author_id, date_creation))
        array_search = cur.fetchall()
        json_array_search = json.dumps(array_search, default=vars, ensure_ascii=False, indent=2)
        return json_array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отдаёт посты, которые находятся у пользователя в избранном
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_wishlist", methods=["POST"])
def get_user_wishlist():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT posts.* FROM posts 
                    join wishlist on wishlist.post_id = posts.id
                    WHERE wishlist.user_id = %s;
                """
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод добавляет пост в избранное для пользователя
# Возвращает (user_id, post_id)
# BODY
# {
#   "user_id": int
#   "post_id": int
# }
@app.route("/api/insert_user_wishlist", methods=["POST"])
def insert_user_wishlist():
    try:
        body = request.json
        user_id = body['user_id']
        post_id = body['post_id']
        query = """ INSERT INTO wishlist (user_id, post_id) 
                    VALUES ( %s, %s)
                    RETURNING wishlist;
                """
        cur.execute(query, (user_id, post_id))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод удаляет пост из избранного у пользователя
# Возвращает []
# BODY
# {
#   "user_id": int
#   "post_id": int
# }
@app.route("/api/delete_user_wishlist", methods=["POST"])
def delete_user_wishlist():
    try:
        body = request.json
        user_id = body['user_id']
        post_id = body['post_id']
        query = """ DELETE FROM wishlist
                    WHERE user_id = %s AND post_id = %s
                    RETURNING wishlist;
                """
        cur.execute(query, (user_id, post_id))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод показывает вакансии, на которые откликнулся пользователь (отклик отменить нельзя)
# Показывает ID автора, название поста, описание поста, мотивационное письмо, ответ автора.
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_orders", methods=["POST"])
def get_user_orders():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT posts.author_id, posts.name, posts.description, orders.motivation_letter, orders.author_response_type
                    FROM posts 
                    join orders on orders.post_id = posts.id
                    WHERE orders.user_id = %s;
                """
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод показывает вакансии, на которые откликнулись пользователи
# Показывает ID автора, название поста, описание поста, мотивационное письмо, ответ автора, ID поста
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_orders_by_author", methods=["POST"])
def get_user_orders_by_author():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT posts.author_id, posts.name, posts.description, orders.motivation_letter, orders.author_response_type, posts.id, orders.user_id
                    FROM posts 
                    join orders on orders.post_id = posts.id
                    WHERE posts.author_id = %s;
                """
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод реализует отклик на вакансию (отклик отменить нельзя)
# "author_response_type": "Отклонили", "Приняли", "На рассмотрении". При отклике автоматически ставится "На рассмотрении"
# Возвращает статус "author_response_type" ("На рассмотрении")
# BODY
# {
#   "user_id": int
#   "post_id": int
#   "motivation_letter": text
# }
@app.route("/api/insert_user_order", methods=["POST"])
def insert_user_order():
    try:
        body = request.json
        user_id = body['user_id']
        post_id = body['post_id']
        motivation_letter = body['motivation_letter']
        query = """ INSERT INTO orders (user_id, post_id, motivation_letter, author_response_type) 
                    VALUES ( %s, %s, %s, 'На рассмотрении')
                    returning author_response_type;
                """
        cur.execute(query, (user_id, post_id, motivation_letter))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отмечает заявку, как Принятую
# "author_response_type": "Отклонили", "Приняли", "На рассмотрении". При отклике автоматически ставится "На рассмотрении"
# Возвращает заявку
# BODY
# {
#   "user_id": int
#   "post_id": int
# }
@app.route("/api/update_author_response_type_for_orders_accepted", methods=["POST"])
def update_author_response_type_for_orders_accepted():
    try:
        body = request.json
        user_id = body['user_id']
        post_id = body['post_id']
        query = """ UPDATE orders SET author_response_type = 'Приняли'
                    WHERE user_id = %s AND post_id = %s
                    RETURNING orders;
                """
        cur.execute(query, (user_id, post_id))
        array_search = cur.fetchall()
        json_array_search = json.dumps(array_search, default=vars, ensure_ascii=False, indent=2)
        return json_array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод отмечает заявку, как Отклоненную
# "author_response_type": "Отклонили", "Приняли", "На рассмотрении". При отклике автоматически ставится "На рассмотрении"
# Возвращает заявку
# BODY
# {
#   "user_id": int
#   "post_id": int
# }
@app.route("/api/update_author_response_type_for_orders_rejected", methods=["POST"])
def update_author_response_type_for_orders_rejected():
    try:
        body = request.json
        user_id = body['user_id']
        post_id = body['post_id']
        query = """ UPDATE orders SET author_response_type = 'Отклонили'
                    WHERE user_id = %s AND post_id = %s
                    RETURNING orders;
                """
        cur.execute(query, (user_id, post_id))
        array_search = cur.fetchall()
        json_array_search = json.dumps(array_search, default=vars, ensure_ascii=False, indent=2)
        return json_array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод показывает отзывы пользователя
# BODY
# {
#   "id": int
# }
@app.route("/api/get_user_reviews", methods=["POST"])
def get_user_reviews():
    try:
        body = request.json
        id = body['id']
        query = """ SELECT author_id, author.name AS author_name, description, reviews.date_creation
                    FROM reviews 
                    JOIN users author on author.id = author_id
                    WHERE addressee_user_id = %s;
                """
        cur.execute(query, (id, ))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# Метод, с помощью которого создаётся отзыв
# Возвращает ID созданного отзыва
# BODY
# {
#   "description": text
#   "author_id": int
#   "addressee_user_id": int
#   "date_creation": timestamp ('2004-10-19 10:23:54')
# }
@app.route("/api/create_reviews", methods=["POST"])
def create_reviews():
    try:
        body = request.json
        description = body['description']
        author_id = body['author_id']
        addressee_user_id = body['addressee_user_id']
        date_creation = body['date_creation']
        query = """ INSERT INTO reviews (description, author_id, addressee_user_id, date_creation) 
                    VALUES ( %s, %s, %s, %s)
                    RETURNING id;
                """
        cur.execute(query, (description, author_id, addressee_user_id, date_creation))
        array_search = cur.fetchall()
        return array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400



if __name__ == '__main__':
    app.run(host='localhost', port=1234, debug=True)
