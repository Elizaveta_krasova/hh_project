import { makeAutoObservable } from 'mobx';
import { addUserWishlistFetch, deleteUserWishlistFetch, getUserReviewsFetch, getUserWishlistFetch } from '../api';
import homeStore from './homeModel';

class FavoriteModel {
  _favorites = [];

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  get favorites() {
    return this._favorites;
  }

  isFavoriteItem(id) {
    return this._favorites?.find((elem) => {
      return elem.id == id;
    });
  }

  async getUserWishlist(id) {
    if (id) {
      await getUserWishlistFetch({ id: Number(id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          const performData = responseJSON?.map((elem) => ({
            id: elem?.[0],
            type: elem?.[1],
            title: elem?.[2],
            description: elem?.[3],
            createdDate: elem?.[6],
            createdId: elem?.[5],
            endDate: elem?.[4],
          }));
          this._favorites = performData;
        });
      });
    }
  }

  async deleteUserWishlist(userId, postId) {
    if (userId !== undefined && postId !== undefined) {
      await deleteUserWishlistFetch(Number(userId), Number(postId)).then(async () => {
        await this.getUserWishlist(userId);
      });
    }
  }

  async addUserWishlist(userId, postId) {
    if (userId !== undefined && postId !== undefined) {
      await addUserWishlistFetch(Number(userId), Number(postId)).then(async () => {
        await this.getUserWishlist(userId);
        await homeStore.fetchPosts();
      });
    }
  }
}

const favoriteStore = new FavoriteModel();
export default favoriteStore;
