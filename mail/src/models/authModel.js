import { makeAutoObservable } from 'mobx';
import { getUserByIdFetch, loginFetch, signFetch } from '../api';
import userStore from './userModel';
import homeStore from './homeModel';
import favoriteStore from './favoriteModel';

class AuthModel {
  token = false;
  id = 0;

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  getCookieValueByName = (name) => {
    const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    return match ? match[2] : '';
  };

  get isAuth() {
    // return this.getCookieValueByName('auth') === 'true' || !!this.token;
    return !!this.token;
  }

  get authId() {
    return this.id;
  }

  async login(data) {
    await loginFetch(data).then(async (response) => {
      await response.json().then((responseJSON) => {
        const id = responseJSON?.[0]?.[0];
        if (id) {
          document.cookie = 'auth=true; max-age=600';
          this.token = true;
          this.id = id;
        }
      });
    });
  }

  async sign(data) {
    await signFetch(data).then(async (response) => {
      await response.json().then((responseJSON) => {
        const id = responseJSON?.[0]?.[0];
        return id;
      });
    });
  }

  async getUserById(id) {
    if (id) {
      await getUserByIdFetch({ id }).then(async (response) => {
        await response.json().then((responseJSON) => {
          if (responseJSON?.[0])
            userStore._user = {
              name: responseJSON?.[0]?.[0] || '',
              date: responseJSON?.[0]?.[1] || '',
              phone: responseJSON?.[0]?.[2] || '',
            };
          homeStore.fetchPosts();
          favoriteStore.getUserWishlist(id);
        });
      });
    }
  }

  logout() {
    document.cookie = 'auth=false';
    this.token = false;
  }
}

const authStore = new AuthModel();
export default authStore;
