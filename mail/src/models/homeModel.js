import { makeAutoObservable } from 'mobx';
import { createOrder, createPostEmployeeFetch, createPostEmployeerFetch, getAllPostsFetch, getPostsEmployeeFetch, getPostsEmployeerFetch } from '../api';
import authStore from './authModel';

class HomeModel {
  _postsEmployee = [];

  _postsEmployeer = [];

  _allPosts = [];

  _allPostsFiltered = [];

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  get postsEmployee() {
    return this._postsEmployee;
  }

  get postsEmployeer() {
    return this._postsEmployeer;
  }

  get allPosts() {
    return this._allPosts;
  }

  searchPosts(value) {
    this._allPostsFiltered = this._allPosts?.filter((elem) => elem.title.includes(value) || elem.description.includes(value));
  }

  async fetchPosts() {
    await getPostsEmployeeFetch().then((response1) => {
      response1.json().then((response1JSON) => {
        const reformData = response1JSON.map((elem) => ({
          id: elem?.[0],
          type: elem?.[1],
          title: elem?.[2],
          description: elem?.[3],
          createdDate: elem?.[6],
          createdId: elem?.[5],
          endDate: elem?.[4],
        }));
        this._postsEmployee = reformData;
      });
    });

    await getPostsEmployeerFetch().then((response2) => {
      response2.json().then((response2JSON) => {
        const reformData = response2JSON.map((elem) => ({
          id: elem?.[0],
          type: elem?.[1],
          title: elem?.[2],
          description: elem?.[3],
          createdDate: elem?.[6],
          createdId: elem?.[5],
          endDate: elem?.[4],
        }));
        this._postsEmployeer = reformData;
      });
    });

    await getAllPostsFetch().then((response2) => {
      response2.json().then((response2JSON) => {
        const reformData = response2JSON.map((elem) => ({
          id: elem?.[0],
          type: elem?.[1],
          title: elem?.[2],
          description: elem?.[3],
          createdDate: elem?.[6],
          createdId: elem?.[5],
          endDate: elem?.[4],
        }));
        this._allPosts = reformData;
      });
    });
  }

  async createPostEmployee(name, description, schedule, author_id, date_creation) {
    await createPostEmployeeFetch(name, description, schedule, author_id, date_creation);
    this.fetchPosts();
  }

  async createPostEmployeer(name, description, schedule, author_id, date_creation) {
    await createPostEmployeerFetch(name, description, schedule, author_id, date_creation);
    this.fetchPosts();
  }

  async sendOrder(text, postId) {
    await createOrder(authStore.authId, postId, text);
  }
}

const homeStore = new HomeModel();
export default homeStore;
