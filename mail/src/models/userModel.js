import { makeAutoObservable } from 'mobx';

class UserModel {
  _user = {
    name: '',
    date: '',
    phone: '',
  };

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  get user() {
    return this._user;
  }
}

const userStore = new UserModel();
export default userStore;
