import { makeAutoObservable, toJS } from 'mobx';
import {
  approveOrderFetch,
  createReviewsFetch,
  denyOrderFetch,
  getOrdersForApprove,
  getOrdersForApproveFetch,
  getUserByIdFetch,
  getUserOrdersFetch,
  getUserPostsFetch,
  getUserReviewsFetch,
} from '../api';
import authStore from './authModel';

class ProfileModel {
  _user = {
    name: '',
    date: '',
    phone: '',
    id: 0,
  };

  _reviews = [];

  _posts = [];

  _orders = [];
  _ordersForApprove = [];

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  async getUserById() {
    if (this._user?.id) {
      await getUserByIdFetch({ id: Number(this._user?.id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          if (responseJSON?.[0])
            this._user = {
              id: this._user?.id,
              name: responseJSON?.[0]?.[0] || '',
              date: responseJSON?.[0]?.[1] || '',
              phone: responseJSON?.[0]?.[2] || '',
            };
        });
      });
    }
  }

  async getUserDataById(id) {
    if (id) {
      await getUserByIdFetch({ id: Number(id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          if (responseJSON?.[0])
            return {
              id: this._user?.id,
              name: responseJSON?.[0]?.[0] || '',
              date: responseJSON?.[0]?.[1] || '',
              phone: responseJSON?.[0]?.[2] || '',
            };
        });
      });
    }
  }

  async getUserPosts() {
    if (this._user?.id) {
      await getUserPostsFetch({ id: Number(this._user?.id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          const reformData = responseJSON.map((elem) => ({
            id: elem?.[0],
            type: elem?.[1],
            title: elem?.[2],
            description: elem?.[3],
            createdDate: elem?.[6],
            createdId: elem?.[5],
            endDate: elem?.[4],
          }));
          this._posts = reformData;
        });
      });
    }
  }

  async getUserReviews() {
    if (this._user?.id) {
      await getUserReviewsFetch({ id: Number(this._user?.id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          const reformData = responseJSON.map((elem) => ({
            fromId: elem?.[0],
            fromName: elem?.[1],
            body: elem?.[2],
            date: elem?.[3],
          }));
          this._reviews = reformData;
        });
      });
    }
  }

  async getUserOrders() {
    if (this._user?.id) {
      await getUserOrdersFetch({ id: Number(this._user?.id) }).then(async (response) => {
        await response.json().then(async (responseJSON) => {
          const reformData = responseJSON.map((elem) => {
            return {
              authorId: elem?.[0],
              title: elem?.[1],
              description: elem?.[2],
              mail: elem?.[3],
              status: elem?.[4],
            };
          });
          this._orders = reformData;
        });
      });
    }
  }

  async getOrdersForApprove() {
    if (this._user?.id) {
      await getOrdersForApproveFetch({ id: Number(this._user?.id) }).then(async (response) => {
        await response.json().then((responseJSON) => {
          const reformData = responseJSON.map((elem) => ({
            authorId: elem?.[0],
            title: elem?.[1],
            description: elem?.[2],
            mail: elem?.[3],
            status: elem?.[4],
            postId: elem?.[5],
            whoseID: elem?.[6],
          }));
          this._ordersForApprove = reformData;
        });
      });
    }
  }

  get user() {
    return this._user;
  }

  get isCurrentUser() {
    return authStore.authId == this._user.id;
  }

  async createReview(comment) {
    if (comment) {
      await createReviewsFetch(comment, authStore.authId, this._user.id, new Date());
      this.update();
    }
  }

  async approveOrder(postId, whoseID) {
    await approveOrderFetch(whoseID, postId);
    this.update();
  }

  async denyOrder(postId, whoseID) {
    await denyOrderFetch(whoseID, postId);
    this.update();
  }

  update() {
    this.getUserPosts();
    this.getUserReviews();
    this.getUserOrders();
    this.getOrdersForApprove();
  }
}

export default ProfileModel;
