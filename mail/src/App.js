import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import HomePage from './views/HomePage';
import LoginPage from './views/LoginPage';
import { ThemeProvider } from './providers/theme.provider';
import { observer } from 'mobx-react-lite';
import authModel from './models/authModel';
import ProfilePage from './views/ProfilePage';
import React from 'react';
import NotFoundPage from './views/NotFoundPage';
import FavoritePage from './views/FavoritePage';
import { LangProvider } from './providers/lang.provider';

const PrivateRoute = observer(({ path, children, ...props }) => {
  if (!authModel.isAuth) {
    return (
      <Redirect
        to={{
          pathname: '/login',
        }}
      />
    );
  }

  return (
    <Route {...props} path={path}>
      {children}
    </Route>
  );
});

const App = () => {
  return (
    <div className="bg-gray-100 dark:bg-gray-900 h-screen overflow-y-auto">
      <LangProvider>
        <ThemeProvider>
          <BrowserRouter>
            <Switch>
              <Route exact path="/login">
                <LoginPage />
              </Route>
              <Route exact path="/not-found">
                <NotFoundPage />
              </Route>
              <PrivateRoute exact path="/home">
                <HomePage />
              </PrivateRoute>
              <PrivateRoute exact path="/profile/:id">
                <ProfilePage />
              </PrivateRoute>
              <PrivateRoute exact path="/favorite">
                <FavoritePage />
              </PrivateRoute>
              <Redirect
                to={{
                  pathname: '/not-found',
                }}
              />
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
      </LangProvider>
    </div>
  );
};
App.displayName = 'App';
export default observer(App);
