import React, { useState, createContext } from 'react';
import { IntlProvider } from 'react-intl';

import { en, ru } from '../lang';

export const LangContext = createContext();

export const LangProvider = ({ children }) => {
  const [lang, setLang] = useState('ru');

  let messages;
  switch (lang) {
    case 'ru':
      messages = ru;
      break;
    case 'en':
    default:
      messages = en;
      break;
  }

  const changeLang = () => {
    if (lang === 'ru') {
      setLang('en');
    } else {
      setLang('ru');
    }
  };

  return (
    <LangContext.Provider value={{ lang, changeLang }}>
      <IntlProvider locale={lang} defaultLocale={lang} messages={messages}>
        {children}
      </IntlProvider>
    </LangContext.Provider>
  );
};
LangProvider.displayName = 'LangProvider';
