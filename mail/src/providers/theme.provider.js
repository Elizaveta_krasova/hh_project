import React, { createContext, useEffect } from 'react';
import useTheme from '../hooks/useTheme';

export const ThemeContext = createContext({});

export const ThemeProvider = (props) => {
  const [theme, changeTheme] = useTheme(props.theme);
  const applyTheme = (nextTheme) => {
    const root = window.document.documentElement;
    const isDark = theme === 'dark';

    root.classList.remove(isDark ? 'light' : 'dark');
    root.classList.add(nextTheme);

    localStorage.setItem('color-scheme', nextTheme);
  };

  useEffect(() => {
    applyTheme(theme);
  }, [theme]);
  return <ThemeContext.Provider value={{ theme, changeTheme }}>{props.children}</ThemeContext.Provider>;
};
ThemeProvider.displayName = 'ThemeProvider';
