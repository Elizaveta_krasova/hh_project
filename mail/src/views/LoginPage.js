import React, { useState } from 'react';
import { LockClosedIcon } from '@heroicons/react/solid';
import { useHistory } from 'react-router-dom';
import authStore from '../models/authModel';
import userStore from '../models/userModel';
import { useIntl } from 'react-intl';

// const tabs = [
//   { name: 'Авторизация', value: 'login' },
//   { name: 'Регистрация', value: 'signup' },
// ];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const LoginPage = () => {
  const { formatMessage } = useIntl();
  const tabs = [
    { name: formatMessage({ id: 'АвторизацияKEY' }), value: 'login' },
    { name: formatMessage({ id: 'РегистрацияKEY' }), value: 'signup' },
  ];
  const history = useHistory();
  const [login, setLogin] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [phone, setPhone] = useState('');
  const [activeTab, setActiveTab] = useState(tabs[0].value);

  if (authStore.isAuth) {
    history.push('/home');
    return;
  }

  const onLogin = async () => {
    await authStore.login({ login, password }).then(() => {
      authStore.getUserById(authStore.authId).then(() => {
        if (userStore._user.name) {
          history.push('/home');
        }
      });
    });
  };

  const onSign = async () => {
    await authStore.sign({ login, password, name, phone });
  };

  return (
    <>
      <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="w-full max-w-md space-y-8">
          <div>
            <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Your Company" />
            <div className="px-6 py-6">
              <div className="px-6 py-6 rounded-lg border border-gray-300 dark:border-gray-700">
                <nav className="flex space-x-4 justify-between" aria-label="Tabs">
                  {tabs.map((tab, index) => (
                    <div
                      onClick={() => setActiveTab(tab.value)}
                      key={index}
                      className={classNames(
                        tab.value === activeTab ? 'text-gray-600 dark:text-gray-200' : 'text-gray-400 dark:text-gray-800',
                        'cursor-pointer px-3 py-2 font-medium text-sm rounded-lg'
                      )}
                    >
                      {tab.name}
                    </div>
                  ))}
                </nav>
              </div>
            </div>{' '}
          </div>
          {activeTab === 'login' && (
            <div className="mt-8 space-y-6">
              <input type="hidden" name="remember" defaultValue="true" />
              <div className="-space-y-px rounded-md shadow-sm">
                <div>
                  <input
                    id="login"
                    name="login"
                    type="login"
                    value={login}
                    onChange={(e) => setLogin(e.target.value)}
                    autoComplete="login"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Логин"
                  />
                </div>
                <div>
                  <input
                    id="password"
                    name="password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    autoComplete="current-password"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Пароль"
                  />
                </div>
              </div>

              <div>
                <button
                  onClick={onLogin}
                  type="button"
                  className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                    <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                  </span>
                  {formatMessage({ id: 'ВойтиKEY' })}
                </button>
              </div>
            </div>
          )}
          {activeTab === 'signup' && (
            <div className="mt-8 space-y-6">
              <input type="hidden" name="remember" defaultValue="true" />
              <div className="-space-y-px rounded-md shadow-sm">
                <div>
                  <input
                    id="login"
                    name="login"
                    type="text"
                    value={login}
                    onChange={(e) => setLogin(e.target.value)}
                    autoComplete="login"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Логин"
                  />
                </div>
                <div>
                  <input
                    id="password"
                    name="password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    autoComplete="current-password"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Пароль"
                  />
                </div>
                <div className="!mt-4">
                  <input
                    id="name"
                    name="name"
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    autoComplete="name"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Имя"
                  />
                </div>
                <div>
                  <input
                    id="phone"
                    name="phone"
                    type="text"
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                    autoComplete="phone"
                    required
                    className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    placeholder="Телефон"
                  />
                </div>
              </div>

              <div>
                <button
                  onClick={onSign}
                  type="button"
                  className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                    <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                  </span>
                  {formatMessage({ id: 'РегистрацияKEY' })}
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};
LoginPage.displayName = 'LoginPage';
export default LoginPage;
