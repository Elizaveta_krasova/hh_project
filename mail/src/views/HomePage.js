import React, { useState } from 'react';
import Header from '../components/Header';
import { StarIcon } from '@heroicons/react/solid';
import favoriteStore from '../models/favoriteModel';
import Modal from '../components/Modal';
import Input from '../components/Input';
import homeStore from '../models/homeModel';
import { Dialog } from '@headlessui/react';
import ModalCreatePost from '../components/ModalCreatePost';
import { toJS } from 'mobx';
import authStore from '../models/authModel';
import { observer } from 'mobx-react-lite';
import { useHistory } from 'react-router-dom';
import { debounce } from '../config/debounce';
import { useIntl } from 'react-intl';

// const tabs = [
//   { name: 'Поиск', value: 'search' },
//   { name: 'Работодателям', value: 'employeer' },
//   { name: 'Соискателям', value: 'employee' },
// ];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const Item = ({ tier }) => {
  const { formatMessage } = useIntl();
  const [open, setOpen] = useState(false);
  const [openOrderModal, setOpenOrderModal] = useState(false);
  const [mail, setMail] = useState('');
  const history = useHistory();
  return (
    <>
      {openOrderModal && (
        <Modal
          open={openOrderModal}
          onClose={() => setOpenOrderModal(false)}
          actions={
            <div className="mt-5 flex flex-col gap-3 sm:mt-6">
              <button
                type="button"
                className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                onClick={() => setOpenOrderModal(false)}
              >
                {formatMessage({ id: 'close' })}
              </button>
              <button
                type="button"
                className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                onClick={() => {
                  homeStore.sendOrder(mail, tier.id);
                  setOpenOrderModal(false);
                }}
              >
                {formatMessage({ id: 'Response' })}
              </button>
            </div>
          }
          body={
            <div className="p-6">
              <input
                type="text"
                name="mail"
                id="mail"
                value={mail}
                onChange={(e) => setMail(e.target.value)}
                className="bg-white dark:bg-gray-800 border border-gray-300 dark:border-gray-700 text-gray-800 dark:text-gray-300 rounded-lg p-4 outline-none block w-full rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                placeholder="Мотивационное письмо"
              />
            </div>
          }
        />
      )}
      {open && (
        <Modal
          open={open}
          onClose={() => setOpen(false)}
          actions={
            <div className="mt-5 flex flex-col gap-3 sm:mt-6">
              <button
                type="button"
                className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                onClick={() => setOpen(false)}
              >
                {formatMessage({ id: 'close' })}
              </button>
              <button
                type="button"
                className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                onClick={() => {
                  history.push(`/profile/${tier.createdId}`);
                  setOpen(false);
                }}
              >
                {formatMessage({ id: 'routeAuthor' })}
              </button>
            </div>
          }
          body={
            <div>
              <div className="mt-3 text-center sm:mt-5">
                <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900 dark:text-gray-100">
                  {tier.title}
                </Dialog.Title>
                <div className="mt-2 text-sm text-gray-500">
                  {formatMessage({ id: 'desc' })}: <p className="">{tier.description}</p>
                </div>
                {tier.createdDate && (
                  <div className="mt-2 text-sm text-gray-500">
                    {formatMessage({ id: 'added' })}: <p className="">{tier.createdDate}</p>
                  </div>
                )}
                {tier.endDate && (
                  <div className="mt-2 text-sm text-gray-500">
                    {formatMessage({ id: 'Expires' })}: <p className="">{tier.endDate}</p>
                  </div>
                )}
              </div>
            </div>
          }
        />
      )}
      <div
        onClick={() => setOpen(true)}
        className="cursor-pointer md:items-center gap-6 md:gap-0 bg-white dark:bg-gray-900 hover:bg-gray-200 dark:hover:bg-gray-800 relative flex flex-col md:flex-row items-start border border-gray-300 dark:border-gray-700 rounded-lg p-8"
      >
        <div className="flex-1">
          <h3 className="text-xl font-semibold text-gray-900 dark:text-gray-300">{tier.title}</h3>
          {tier.endDate ? (
            <p className="absolute top-0 -translate-y-1/2 transform rounded-full bg-indigo-500 py-1.5 px-4 text-sm font-semibold text-white">
              {new Date(tier.endDate) < new Date() ? formatMessage({ id: 'ranOut' }) : formatMessage({ id: 'WillEndSoon' })}
            </p>
          ) : null}
          <p className="mt-6 text-gray-500">{tier.description}</p>
        </div>

        <div className="flex gap-6 items-center">
          {!favoriteStore.isFavoriteItem(tier.id) && (
            <StarIcon
              onClick={(e) => {
                e.stopPropagation();
                favoriteStore.addUserWishlist(authStore.authId, tier.id);
              }}
              className="cursor-pointer text-yellow-500 dark:text-yellow-600 dark:hover:text-yellow-400 hover:text-yellow-300 h-10 w-10"
            />
          )}

          {new Date(tier.endDate) > new Date() && (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setOpenOrderModal(true);
              }}
              className="max-w-[200px] bg-indigo-500 text-white hover:bg-indigo-600 block w-full py-3 px-6 border border-transparent rounded-md text-center font-medium"
            >
              {formatMessage({ id: 'respond' })}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

const HomePage = () => {
  const { formatMessage } = useIntl();
  const tabs = [
    { name: formatMessage({ id: 'searchKEY' }), value: 'search' },
    { name: formatMessage({ id: 'EmployeerKEY' }), value: 'employeer' },
    { name: formatMessage({ id: 'EmployeeKEY' }), value: 'employee' },
  ];
  const [activeTab, setActiveTab] = useState(tabs[0].value);
  const [openEmployeer, setOpenEmployeer] = useState(false);
  const [openEmployee, setOpenEmployee] = useState(false);
  const [valueSearch, setValueSearch] = useState('');

  return (
    <>
      {openEmployee && (
        <ModalCreatePost
          onCreate={async (name, description, schedule) => {
            await homeStore.createPostEmployee(name, description, schedule, Number(authStore.authId), new Date());
            setOpenEmployee(false);
          }}
          type="employee"
          onClose={() => setOpenEmployee(false)}
        />
      )}
      {openEmployeer && (
        <ModalCreatePost
          onCreate={async (name, description, schedule) => {
            await homeStore.createPostEmployeer(name, description, schedule, Number(authStore.authId), new Date());
            setOpenEmployeer(false);
          }}
          type="employeer"
          onClose={() => setOpenEmployeer(false)}
        />
      )}
      <Header />
      <div className="px-6 py-6">
        <div className="px-6 py-6 rounded-lg border border-gray-300 dark:border-gray-700">
          <nav className="flex space-x-4" aria-label="Tabs">
            {tabs.map((tab, index) => (
              <div
                onClick={() => setActiveTab(tab.value)}
                key={index}
                className={classNames(
                  tab.value === activeTab ? 'text-gray-600 dark:text-gray-200' : 'text-gray-400 dark:text-gray-800',
                  'cursor-pointer px-3 py-2 font-medium text-sm rounded-lg'
                )}
              >
                {tab.name}
              </div>
            ))}
          </nav>
        </div>
      </div>
      {activeTab === 'search' && (
        <div className="flex flex-col gap-2">
          <Input
            placeholder="Поиск постов"
            name="autocomplete"
            value={valueSearch}
            onChange={(e) => {
              setValueSearch(e.target.value);
              homeStore.searchPosts(e.target.value);
            }}
          />
          <div className="px-6 flex flex-col gap-6">
            {(valueSearch ? homeStore._allPostsFiltered : homeStore.allPosts).map((tier, index) => (
              <Item key={index} tier={tier} />
            ))}
          </div>
        </div>
      )}
      {activeTab !== 'search' && (
        <>
          <div
            onClick={async () => {
              if (activeTab === 'employeer') {
                setOpenEmployeer(true);
              } else {
                setOpenEmployee(true);
              }
            }}
            className="mx-6 bg-indigo-500 text-white hover:bg-indigo-600 block py-3 border border-transparent rounded-md text-center font-medium"
          >
            {formatMessage({ id: 'create' })}
          </div>
          <div className="px-6 py-10 flex flex-col gap-6">
            {(activeTab === 'employeer' ? homeStore.postsEmployeer : homeStore.postsEmployee)?.map((tier, index) => (
              <Item key={index} tier={tier} />
            ))}
          </div>
        </>
      )}
    </>
  );
};
HomePage.displayName = 'HomePage';
export default observer(HomePage);
