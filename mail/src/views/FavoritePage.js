import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import { useHistory, useParams } from 'react-router-dom';
import favoriteStore from '../models/favoriteModel';
import authStore from '../models/authModel';
import { toJS } from 'mobx';
import { observer } from 'mobx-react-lite';
import Modal from '../components/Modal';
import { Dialog } from '@headlessui/react';
import { useIntl } from 'react-intl';

const FavoritePage = () => {
  const [open, setOpen] = useState(false);
  const { formatMessage } = useIntl();
  const history = useHistory();
  return (
    <>
      <Header />
      <div className="px-6 py-10">
        <div className="border border-gray-300 dark:border-gray-700 rounded-lg divide-y divide-gray-300 dark:divide-gray-700">
          <div className="px-4 py-5 sm:px-6">
            <h2 id="notes-title" className="text-lg font-medium text-gray-900 dark:text-gray-500">
              {formatMessage({ id: 'favorite' })}
            </h2>
          </div>
          <div className="">
            <ul role="list">
              {favoriteStore.favorites?.map((item, index) => (
                <li
                  onClick={() => setOpen(true)}
                  key={index}
                  className="cursor-pointer px-4 py-6 sm:px-6 flex flex-col gap-6 md:flex-row md:gap-0 md:justify-between hover:bg-gray-200 dark:hover:bg-gray-800"
                >
                  {open && (
                    <Modal
                      open={open}
                      onClose={() => setOpen(false)}
                      actions={
                        <div className="mt-5 flex flex-col gap-3 sm:mt-6">
                          <button
                            type="button"
                            className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                            onClick={() => setOpen(false)}
                          >
                            {formatMessage({ id: 'close' })}
                          </button>
                          <button
                            type="button"
                            className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                            onClick={() => {
                              history.push(`/profile/${item.createdId}`);
                              setOpen(false);
                            }}
                          >
                            {formatMessage({ id: 'routeAuthor' })}
                          </button>
                        </div>
                      }
                      body={
                        <div>
                          <div className="mt-3 text-center sm:mt-5">
                            <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900 dark:text-gray-100">
                              {item.title}
                            </Dialog.Title>
                            <div className="mt-2 text-sm text-gray-500">
                              {formatMessage({ id: 'desc' })} <p className="">{item.description}</p>
                            </div>
                            {item.createdDate && (
                              <div className="mt-2 text-sm text-gray-500">
                                {formatMessage({ id: 'added' })} <p className="">{item.createdDate}</p>
                              </div>
                            )}
                            {item.endDate && (
                              <div className="mt-2 text-sm text-gray-500">
                                {formatMessage({ id: 'Expires' })} <p className="">{item.endDate}</p>
                              </div>
                            )}
                          </div>
                        </div>
                      }
                    />
                  )}
                  <div className="flex space-x-3">
                    <div>
                      <div className="text-sm">
                        <div className="font-medium text-gray-900 dark:text-gray-500">{item.title}</div>
                      </div>
                      <div className="mt-1 text-sm text-gray-700">
                        <p>{item.description}</p>
                      </div>
                    </div>
                  </div>
                  <button
                    onClick={(e) => {
                      e.stopPropagation();
                      favoriteStore.deleteUserWishlist(authStore.authId, item.id);
                    }}
                    type="button"
                    className="py-2 text-xs max-w-[200px] bg-indigo-500 text-white hover:bg-indigo-600 blockpy-3 px-6 border border-transparent rounded-md text-center font-medium"
                  >
                    {formatMessage({ id: 'deleteToWishlist' })}
                  </button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};
FavoritePage.displayName = 'FavoritePage';
export default observer(FavoritePage);
