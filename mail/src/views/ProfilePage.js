import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import { QuestionMarkCircleIcon } from '@heroicons/react/outline';
import ProfileModel from '../models/profileModel';
import { useHistory, useParams } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { createReviewsFetch } from '../api';
import { useIntl } from 'react-intl';

const ProfilePage = () => {
  const { formatMessage } = useIntl();
  const history = useHistory();
  const { id } = useParams();
  const [profileModel] = useState(new ProfileModel());
  const [comment, setComment] = useState('');

  useEffect(() => {
    if (id) {
      profileModel._user.id = id;
      profileModel.getUserById();
      profileModel.getUserPosts();
      profileModel.getUserReviews();
      profileModel.getUserOrders();
    }
  }, [id]);

  return (
    <>
      <Header />
      <div className="px-6 py-10">
        <div className="mx-auto md:flex md:items-center md:justify-between md:space-x-5">
          <div className="flex items-center space-x-5">
            <div className="flex-shrink-0">
              <div className="relative">
                <img
                  className="h-16 w-16 rounded-full"
                  src="https://avatars.mds.yandex.net/i?id=72155d0e963d84da9fe4f28a60046be2f0caa6c2-6402780-images-thumbs&n=13"
                  alt=""
                />
                <span className="absolute inset-0 rounded-full shadow-inner" aria-hidden="true" />
              </div>
            </div>
            <div>
              <h1 className="text-2xl font-bold text-gray-900 dark:text-gray-400">{profileModel?.user?.name}</h1>
              <h1 className="text-xl font-bold text-gray-900 dark:text-gray-400">{profileModel?.user?.phone}</h1>
              <p className="text-sm font-medium text-gray-500">Аккаунт создан {profileModel?.user?.date}</p>
            </div>
          </div>
          <div className="mt-6 md:mt-0 flex gap-3">
            <button
              onClick={profileModel.update}
              type="button"
              className="inline-flex items-center justify-center rounded-md border border-gray-300 dark:border-gray-700 bg-white dark:bg-gray-900 px-4 py-2 text-sm font-medium text-gray-700 dark:text-gray-400 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
            >
              {formatMessage({ id: 'updateDate' })}
            </button>
            <button
              onClick={() => history.push('/home')}
              type="button"
              className="inline-flex items-center justify-center rounded-md border border-gray-300 dark:border-gray-700 bg-white dark:bg-gray-900 px-4 py-2 text-sm font-medium text-gray-700 dark:text-gray-400 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
            >
              {formatMessage({ id: 'back' })}
            </button>
          </div>
        </div>

        <div className="mx-auto mt-8 gap-6">
          <div className="flex flex-col gap-6">
            {profileModel.isCurrentUser && (
              <div className="border border-gray-300 dark:border-gray-700 rounded-lg divide-y divide-gray-300 dark:divide-gray-700">
                <div className="px-4 py-5 sm:px-6">
                  <h2 id="notes-title" className="text-lg font-medium text-gray-900 dark:text-gray-500">
                    {formatMessage({ id: 'ResponsesForYou' })}
                  </h2>
                </div>
                <div className="">
                  <ul role="list" className="">
                    {profileModel._ordersForApprove?.map((order, index) => (
                      <li key={index} className="px-4 py-6 sm:px-6 hover:bg-gray-200 dark:hover:bg-gray-800">
                        <div className="flex justify-between items-center">
                          <div>
                            <div className="text-sm">
                              <div className="font-medium text-gray-900 dark:text-gray-500">{order.title}</div>
                            </div>
                            <div className="mt-1 text-sm text-gray-700">
                              <p>{order.description}</p>
                            </div>
                            <div className="mt-1 text-sm text-gray-700">
                              <p>{order.mail}</p>
                            </div>
                          </div>
                          {order.status === 'На рассмотрении' ? (
                            <div className="flex gap-3">
                              <div className="mt-3 flex items-center justify-between">
                                {console.log('==========>order.whoseID', order.whoseID)}
                                <button
                                  onClick={() => profileModel.approveOrder(order.postId, order.whoseID)}
                                  type="button"
                                  className="inline-flex items-center justify-center rounded-md border border-transparent bg-green-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                  {formatMessage({ id: 'Approve' })}
                                </button>
                              </div>
                              <div className="mt-3 flex items-center justify-between">
                                <button
                                  onClick={() => profileModel.denyOrder(order.postId, order.whoseID)}
                                  type="button"
                                  className="inline-flex items-center justify-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2"
                                >
                                  {formatMessage({ id: 'Reject' })}
                                </button>
                              </div>
                            </div>
                          ) : (
                            <div className="p-3 text-center border-2 rounded-lg border-indigo-400 w-[200px] text-gray-800 dark:text-white">{order.status}</div>
                          )}
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            )}

            {profileModel.isCurrentUser && (
              <div className="border border-gray-300 dark:border-gray-700 rounded-lg divide-y divide-gray-300 dark:divide-gray-700">
                <div className="px-4 py-5 sm:px-6">
                  <h2 id="notes-title" className="text-lg font-medium text-gray-900 dark:text-gray-500">
                    {formatMessage({ id: 'MyResponses' })}
                  </h2>
                </div>
                <div className="">
                  <ul role="list" className="">
                    {profileModel._orders?.map((order, index) => (
                      <li key={index} className="px-4 py-6 sm:px-6 hover:bg-gray-200 dark:hover:bg-gray-800">
                        <div className="flex justify-between items-center">
                          <div>
                            <div className="text-sm">
                              <div className="font-medium text-gray-900 dark:text-gray-500">{order.title}</div>
                            </div>
                            <div className="mt-1 text-sm text-gray-700">
                              <p>{order.description}</p>
                            </div>
                            <div className="mt-1 text-sm text-gray-700">
                              <p>{order.mail}</p>
                            </div>
                            {index === 0 && (
                              <div className="mt-1 text-sm text-gray-700 flex gap-2">
                                Телефон для связи: <p>89896241896</p>
                              </div>
                            )}{' '}
                            {index === 1 && (
                              <div className="mt-1 text-sm text-gray-700 flex gap-2">
                                Телефон для связи: <p>89641837465</p>
                              </div>
                            )}{' '}
                          </div>
                          <div className="p-3 text-center border-2 rounded-lg border-indigo-400 w-[200px] text-gray-800 dark:text-white">{order.status}</div>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            )}

            <div className="border border-gray-300 dark:border-gray-700 rounded-lg divide-y divide-gray-300 dark:divide-gray-700">
              <div className="px-4 py-5 sm:px-6">
                <h2 id="notes-title" className="text-lg font-medium text-gray-900 dark:text-gray-500">
                  {formatMessage({ id: 'Posts' })}
                </h2>
              </div>
              <div className="">
                <ul role="list" className="">
                  {profileModel._posts?.map((comment, index) => (
                    <li key={index} className="px-4 py-6 sm:px-6 hover:bg-gray-200 dark:hover:bg-gray-800">
                      <div className="flex space-x-3">
                        <div>
                          <div className="text-sm">
                            <div className="font-medium text-gray-900 dark:text-gray-500">{comment.title}</div>
                          </div>
                          <div className="mt-1 text-sm text-gray-700">
                            <p>{comment.description}</p>
                          </div>
                        </div>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className="border border-gray-300 dark:border-gray-700 rounded-lg divide-y divide-gray-300 dark:divide-gray-700">
              <div className="px-4 py-5 sm:px-6">
                <h2 id="notes-title" className="text-lg font-medium text-gray-900 dark:text-gray-500">
                  {formatMessage({ id: 'Reviews' })}
                </h2>
              </div>
              <div className="">
                <ul role="list" className="">
                  {profileModel._reviews?.map((comment, index) => (
                    <li key={index} className="px-4 py-6 sm:px-6 hover:bg-gray-200 dark:hover:bg-gray-800">
                      <div className="flex space-x-3">
                        <div>
                          <div className="text-sm">
                            <a href="#" className="font-medium text-gray-900 dark:text-gray-500">
                              {comment.fromName}
                            </a>
                          </div>
                          <div className="mt-1 text-sm text-gray-700">
                            <p>{comment.body}</p>
                          </div>
                          <div className="mt-2 space-x-2 text-sm">
                            <span className="font-medium text-gray-500">{comment.date}</span>
                          </div>
                        </div>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            {!profileModel.isCurrentUser && (
              <div className="rounded-lg bg-gray-50 dark:bg-gray-800 px-4 py-6 sm:px-6">
                <div className="min-w-0 flex-1">
                  <div>
                    <label htmlFor="comment" className="sr-only">
                      About
                    </label>
                    <textarea
                      id="comment"
                      name="comment"
                      value={comment}
                      rows={3}
                      onChange={(e) => setComment(e.target.value)}
                      className="p-3 block w-full dark:bg-gray-900 rounded-md border-gray-300 shadow-sm focus:border-blue-500 focus:ring-blue-500 sm:text-sm dark:text-gray-300 outline-none"
                      placeholder="Отзыв"
                      defaultValue={''}
                    />
                  </div>
                  <div className="mt-3 flex items-center justify-between">
                    <button
                      onClick={() => profileModel.createReview(comment)}
                      type="button"
                      className="inline-flex items-center justify-center rounded-md border border-transparent bg-blue-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
                    >
                      {formatMessage({ id: 'AddedReview' })}
                    </button>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
ProfilePage.displayName = 'ProfilePage';
export default observer(ProfilePage);
