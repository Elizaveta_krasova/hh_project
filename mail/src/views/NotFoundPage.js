import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';

const NotFoundPage = () => {
  return (
    <>
      <Header />
      <div className="flex items-center justify-center w-screen h-[calc(100vh-56px)]">
        <div className="px-40 py-20 bg-white dark:bg-gray-800 rounded-md shadow-xl">
          <div className="flex flex-col items-center">
            <h1 className="font-bold text-blue-600 text-9xl">404</h1>

            <h6 className="mb-2 text-2xl font-bold text-center text-gray-800 dark:text-gray-300 md:text-3xl">
              <span className="text-red-500">Уупс!</span> Страница не найдена
            </h6>

            <p className="mb-8 text-center text-gray-500 md:text-lg">Такой страницы не существует</p>
            <Link
              to="home"
              className="inline-flex items-center justify-center rounded-md border border-gray-300 dark:border-gray-700 bg-white dark:bg-gray-900 px-4 py-2 text-sm font-medium text-gray-700 dark:text-gray-400 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
            >
              Домой
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};
NotFoundPage.displayName = 'NotFoundPage';
export default NotFoundPage;
