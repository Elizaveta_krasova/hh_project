import RU from './ru.json';
import EN from './en.json';

export const ru = { ...RU };
export const en = { ...EN };
