import React, { useCallback, useEffect, useState } from 'react';

const EThemes = {
  dark: 'dark',
  light: 'light',
};

const useTheme = () => {
  const [theme, setTheme] = useState(EThemes.light);

  const changeTheme = useCallback(() => {
    if (theme === EThemes.light) {
      setTheme(EThemes.dark);
      return;
    }
    setTheme(EThemes.light);
  }, [theme]);

  useEffect(() => {
    window.localStorage.setItem('theme', theme);
  }, [theme]);

  useEffect(() => {
    const localStorageTheme = window.localStorage.getItem('theme');
    if (localStorageTheme) {
      setTheme(localStorageTheme);
    }
  }, []);

  return [theme, changeTheme];
};

export default useTheme;
