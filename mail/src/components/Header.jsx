import { LightBulbIcon } from '@heroicons/react/solid';
import React, { useContext } from 'react';
import { ThemeContext } from '../providers/theme.provider';
import { Link, useHistory } from 'react-router-dom';
import authModel from '../models/authModel';
import { useIntl } from 'react-intl';
import { LangContext } from '../providers/lang.provider';
import authStore from '../models/authModel';
import favoriteStore from '../models/favoriteModel';
import homeStore from '../models/homeModel';

const Header = () => {
  const { formatMessage } = useIntl();
  const history = useHistory();
  const { changeTheme } = useContext(ThemeContext);
  const { changeLang, lang } = useContext(LangContext);

  return (
    <header className="flex h-14 bg-white dark:bg-gray-800 px-4 py-3 justify-between items-center gap-6">
      <div className="flex gap-2 text-sm font-medium text-gray-900 dark:text-gray-300">
        <Link to="/home">{formatMessage({ id: 'main' })}</Link>
      </div>
      <div className="flex gap-6">
        <div className="flex gap-2 text-sm font-medium text-gray-900 dark:text-gray-300">
          <Link to="/favorite">{formatMessage({ id: 'favorites' })}</Link>
        </div>
        <div className="flex gap-2 text-sm font-medium text-gray-900 dark:text-gray-300">
          <Link to={`/profile/${authStore.authId}`}>{formatMessage({ id: 'profile' })}</Link>
        </div>
        <div
          onClick={() => {
            authModel.logout();
            history.push('/login');
          }}
          className="cursor-pointer flex gap-2 text-sm font-medium text-gray-900 dark:text-gray-300"
        >
          {formatMessage({ id: 'exit' })}
        </div>
        <div
          onClick={() => {
            favoriteStore.getUserWishlist(authStore.authId);
            homeStore.fetchPosts();
          }}
          className="cursor-pointer flex gap-2 text-sm font-medium text-gray-900 dark:text-gray-300"
        >
          <Link to={`/profile/${authStore.authId}`}>{formatMessage({ id: 'updateDate' })}</Link>
        </div>
        <LightBulbIcon onClick={changeTheme} className="h-5 w-5 text-gray-900 dark:text-gray-300" aria-hidden="true" />
        <div onClick={changeLang} className="uppercase text-sm font-medium text-gray-900 dark:text-gray-300">
          {lang}
        </div>
      </div>
    </header>
  );
};
Header.displayName = 'Header';

export default Header;
