import Modal from './Modal';
import React, { useState } from 'react';
import { useIntl } from 'react-intl';

const ModalCreatePost = ({ onClose, onCreate }) => {
  const { formatMessage } = useIntl();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [schedule, setSchedule] = useState(new Date());

  const handleCreate = () => {
    onCreate(name, description, schedule);
  };

  return (
    <Modal
      open
      onClose={onClose}
      actions={
        <div className="mt-5 sm:mt-6 flex gap-4">
          <button
            type="button"
            className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
            onClick={onClose}
          >
            {formatMessage({ id: 'close' })}
          </button>
          <button
            type="button"
            className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
            onClick={handleCreate}
          >
            {formatMessage({ id: 'create' })}
          </button>
        </div>
      }
      body={
        <div className="flex flex-col gap-4">
          <input
            id="name"
            name="name"
            type="text"
            onChange={(e) => setName(e.target.value)}
            autoComplete="name"
            className="dark:bg-transparent dark:text-white relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
            placeholder="Название"
          />
          <input
            id="description"
            name="description"
            type="text"
            onChange={(e) => setDescription(e.target.value)}
            autoComplete="description"
            className="dark:bg-transparent dark:text-white relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
            placeholder="Описание"
          />
          <input
            id="schedule"
            name="schedule"
            type="date"
            onChange={(e) => {
              setSchedule(new Date(e.target.value));
            }}
            className="dark:bg-transparent dark:text-white relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
            placeholder="Расписание"
          />
        </div>
      }
    />
  );
};

export default ModalCreatePost;
