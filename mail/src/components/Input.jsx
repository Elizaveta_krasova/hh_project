const Input = (props) => {
  return (
    <div className="p-6">
      {props.label && (
        <label htmlFor={props.name} className="block text-sm font-medium text-gray-700">
          {props.label}
        </label>
      )}
      <div className="mt-1">
        <input
          type="text"
          name={props.name}
          id={props.name}
          value={props.value}
          className="bg-white dark:bg-gray-800 border border-gray-300 dark:border-gray-700 text-gray-800 dark:text-gray-300 rounded-lg p-4 outline-none block w-full rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
          placeholder={props.placeholder}
          {...props}
        />
      </div>
    </div>
  );
};
Input.displayName = 'Input';
export default Input;
