import { DEV_API } from '../config';

export const loginFetch = (data) => {
  return fetch(`${DEV_API}/api/login`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
};

export const getUserByIdFetch = (data) => {
  return fetch(`${DEV_API}/api/get_user_by_id`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
};

export const signFetch = (data) => {
  return fetch(`${DEV_API}/api/create_user`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ ...data, date_creation: new Date() }),
  });
};

export const getPostsEmployeeFetch = () => {
  return fetch(`${DEV_API}/api/get_posts`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },

    body: JSON.stringify({ type: 'employee' }),
  });
};

export const getPostsEmployeerFetch = () => {
  return fetch(`${DEV_API}/api/get_posts`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ type: 'employeer' }),
  });
};

export const createPostEmployeeFetch = (name, description, schedule, author_id, date_creation) => {
  return fetch(`${DEV_API}/api/create_post`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ type: 'employee', name, description, schedule, author_id, date_creation }),
  });
};
export const createPostEmployeerFetch = (name, description, schedule, author_id, date_creation) => {
  return fetch(`${DEV_API}/api/create_post`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ type: 'employeer', name, description, schedule, author_id, date_creation }),
  });
};
export const getUserPostsFetch = (id) => {
  return fetch(`${DEV_API}/api/get_user_posts`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
};

export const getUserReviewsFetch = (id) => {
  return fetch(`${DEV_API}/api/get_user_reviews`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
};

export const getUserWishlistFetch = (id) => {
  return fetch(`${DEV_API}/api/get_user_wishlist`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
};

export const addUserWishlistFetch = (userId, postId) => {
  return fetch(`${DEV_API}/api/insert_user_wishlist`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user_id: userId, post_id: postId }),
  });
};

export const deleteUserWishlistFetch = (userId, postId) => {
  return fetch(`${DEV_API}/api/delete_user_wishlist`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user_id: Number(userId), post_id: Number(postId) }),
  });
};

export const getUserOrdersFetch = (id) => {
  return fetch(`${DEV_API}/api/get_user_orders`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
};

export const getAllPostsFetch = () => {
  return fetch(`${DEV_API}/api/get_all_posts`, {
    method: 'GET',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
  });
};

export const createReviewsFetch = (description, authorId, receiverId, date) => {
  return fetch(`${DEV_API}/api/create_reviews`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      description: String(description),
      author_id: Number(authorId),
      addressee_user_id: Number(receiverId),
      date_creation: new Date(date),
    }),
  });
};

export const createOrder = (userId, postId, text) => {
  return fetch(`${DEV_API}/api/insert_user_order`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      user_id: Number(userId),
      post_id: Number(postId),
      motivation_letter: String(text),
    }),
  });
};

export const getOrdersForApproveFetch = (userId) => {
  return fetch(`${DEV_API}/api/get_user_orders_by_author`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userId),
  });
};

export const approveOrderFetch = (userId, postId) => {
  console.log('==========>userId, postId', userId, postId);
  return fetch(`${DEV_API}/api/update_author_response_type_for_orders_accepted`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user_id: userId, post_id: postId }),
  });
};

export const denyOrderFetch = (userId, postId) => {
  return fetch(`${DEV_API}/api/update_author_response_type_for_orders_rejected`, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': 'no-cors',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user_id: userId, post_id: postId }),
  });
};
