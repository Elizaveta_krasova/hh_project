export const getEnvVar = (key) => {
  if (process.env[key] === undefined) {
    throw new Error(`Env variable ${key} is required`);
  }
  return process.env[key] || '';
};

export const DEV_API = getEnvVar('REACT_APP_API_URL');
